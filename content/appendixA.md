## Exempel i CSV, kommaseparerad

Nedan är första raden i en fil som representerar [Lilla Ekarängens lekplats i Borås Stad](https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen). Observera att flera fält är tomma och då helt saknar innehåll. Istället är de tänkta värdena ersatta med ett komma. Att använda komma som separator är rekommenderat för att datamängden ska enkelt vara internationellt användbar.

Flera värden är tomma, t.ex. adressfälten eftersom platsen saknar gatunamn och husnummer, men däremot finns postadressen Borås, så den är ifylld. Om du inte vet om ifall en viss egenskap finns på platsen, t.ex. “sandpit” (sandlåda) så ska du inte skriva “false” utan du ska lämna fältet tomt. Tomt fält betyder att du ej vet eller ej har inventerat. 

Om du har flera fält som du lämnar tomma, kanske det är dags att kommunen inventerar lekplatsen på nytt för att kunna förse datakonsumenten med så aktuell data som möjligt. 

<div class="example csvtext">
source,id,name,type,latitude,longitude,email,alt_source,wikidata,visit_url,updated,description,street,housenumber,postcode,city,country,lighting,structure,sandpit,swing,basketswing,zipwire,slide,roundabout,springy,toilet,drinking_water,wheelchair,accessibility,td_url<br>
2120001561,0290-Boras,Lilla Ekarangens lekplats,N,57.719413,12.982124,tekniska@boras.se,1490,,https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen ,2022-02-15,Lilla Ekarängens lekplats är stor till ytan och inbjuder till rörelse i olika former. Här kan man dansa och tävla under dansbågen eller öva sin balans styrka och smidighet i hinderbanan. Beläget nära Ekarängsskolan.,,,Borås,SE,true,false,true,false,true,false,true,false,true,false,false,false,true,yes
</div>

## Exempel i CSV, semikolonseparerad

Nedan är första raden i en fil som representerar Lilla Ekarängens lekplats i Borås Stad. Observera att flera fält är tomma och då helt saknar innehåll, istället är de tänkta värdena ersatta med ett semikolon.

<div class="example csvtext">
source;id;name;type;latitude;longitude;email;alt_source;wikidata;visit_url;updated;description;street;housenumber;postcode;city;country;lighting;structure;sandpit;swing;basketswing;zipwire;slide;roundabout;springy;toilet;drinking_water;wheelchair;accessibility;td_url<br>
2120001561;0290-Boras;Lilla Ekarangens lekplats;N;57.719413;12.982124;tekniska@boras.se;1490;;https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen ;2022-02-15;Lilla Ekarängens lekplats är stor till ytan och inbjuder till rörelse i olika former. Här kan man dansa och tävla under dansbågen eller öva sin balans, styrka och smidighet i hinderbanan. Beläget nära Ekarängsskolan.;;;Borås;SE;true;false;true;false;true;false;true;false;true;false;false;false;true;yes
</div>
