# Exempel i JSON

Samma lekplats som i Appendix A, fast nu som JSON.

```json
{
    "source": "2120001264",
    "id":  "0290-Boras",
    "name":  "Lilla Ekarangens lekplats",
    "type": "N",
    "latitude":  "57.719413",
    "longitude":  "12.982124",
    "email":  "tekniska@boras.se",
    "alt_source":  "1490",
    "wikidata":  "",
    "visit_url":  "https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen",
    "updated":  "2022-02-15",
    "description":  "Lilla Ekarängens lekplats är stor till ytan och inbjuder till rörelse i olika former. Här kan man dansa och tävla under dansbågen eller öva sin balans, styrka och smidighet i hinderbanan. Beläget nära Ekarängsskolan.",
    "street":  "",
    "housenumber":  "",
    "postcode":  "",
    "city":  "Borås",
    "country":  "SE",
    "lighting":  "true",
    "structure":  "false",
    "sandpit":  "true",
    "swing":  "false",
    "basketswing":  "true",
    "zipwire":  "false",
    "slide":  "true",
    "roundabout":  "false",
    "springy":  "true",
    "drinking_water":  "false",
    "wheelchair":  "yes",
    "accessibility":  "",
    "td_url":  ""
}
```