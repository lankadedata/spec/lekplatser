# Länkar

## Wikidata

På initiativet [Wikidata](https://www.wikidata.org) görs ett gediget arbete för att kunna länka och bygga kunskap utefter data. Genom att göra den här specifikationen kompatibel med Wikidata genom länkning hoppas vi kunna bidra till att höja kvaliteten i Wikidata och i specifikationen. Vill du att dina lekplatser i kommunen syns bättre i dessa plattformar - se till att bilder på lekplatsen finns tillgängliga med med fri licens som [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.sv) eller ladda upp bilder på [WikiCommons](https://commons.wikimedia.org/wiki/Main_Page).

Använd fältet “wikidata” i specifikationens [datamodell](#datamodell) för att länka till det objekt i Wikidata som beskriver lekplatsen. Det kan komma att finnas ett unikt ID i Wikidata för varje lekplats.

ID i Wikidata anges med en så kallad “Q-kod” och börjar med bokstaven Q och följs sedan av ett antal siffror. Detta är det unika ID som lekplatsen har i Wikidatas databas. Om det inte finns en Q-kod för din lekplats, rekommenderas du att lägga till lekplatsen på Wikidata och länka dit.

Om lekplatsen finns med i Wikidata öppnas fler möjligheter t.ex. att den enklare kan visas i sökresultat från sökmotorer eller i kartapplikationer. En rekommendationen är därför att du skapar ett objekt på Wikidata som beskriver din lekplats. Generellt saknas flertalet lekplatser i Wikidata. 

## OpenStreetMap

Under arbetet med denna specifikation valde vi att utgå från OpenStreetMaps mappning gällande lekplatser och dess anordningar. Vi önskade att specifikationen för lekplatser blir kompatibel med och kan användas av gemenskapen på bästa sätt. 

## Tillgänglighetsdatabasen

Tillgänglighetsdatabasen (TD) saknar persistenta identifierare som är stabila över tid och det går inte att hänvisa till ett objekt på ett stabilt sätt annat än med hjälp av URL direkt till objektet som beskrivs. Tips har lämnats till TD om att en sådan identifierare kanske kunde skapas och göras synlig/tydlig i API och på webbplatsen, så att länkning av data kan ske på ett bra sätt. 

TD har ett API som du kan nå på
[Tillgänglighetsdatabasen - developer portal (azure-api.net)](https://td.portal.azure-api.net/) och som är kostnadsfritt. För att använda API:erna måste du dock registrera dig och hämta ut en personlig nyckel. APIt och dess utveckling sköts helt av Tillgänglighetsdatabasen och frågor om dess kapabilitet och funktion hänvisar vi till TD. Lämna gärna förbättringsförslag till TD som har indikerat till oss att de är intresserade av att deras tjänster nyttjas och utvecklas.

### Min lekplats saknas i Tillgänglighetsdatabasen

Kontakta din kommun där lekplatsen ligger och be dem lägga till lekplatsen. Generellt kan en del lekplatser som inte finns i kommunens ägor saknas. Se kommunens kontaktuppgifter till datamängden lekplatser.