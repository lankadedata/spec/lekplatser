
**Bakgrund**

Syftet till specifikationen är att presentera information om lekplatser på att enkelt och likvärdigt sätt.

Specifikationen syftar till att ge publicister i Västra Götalandsregionen och Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver lekplatser på ett enhetligt sätt. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Vi har också haft stöd från följande organisationer:

**[Västra Götalandsregionen](https://www.vgregion.se/)** - Behov och bidrag.<br>
**[MetaSolutions](https://entryscape.com/)** - Tips, stöd och råd, samt teknisk bas för specifikationshantering.<br>
