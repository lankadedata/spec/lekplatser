# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en lekplats och varje kolumn motsvarar en egenskap för denna lekplats. 31 attribut är definierade, där de första 7 är obligatoriska. Speciellt viktigt är att det finns en identifierare (kolumnen “id”) då det möjliggör att man på ett unikt sätt kan referera till enskilda lekplatser. Identifieraren ska vara unik och beständigt över tid och återanvändning av identifieraren avråds från (t.ex. om en lekplats läggs ned). Se [förtydliganden](#fortydligande-attribut).

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Orsaken till detta är att man vill använda kolumnnamn snarare än ordningen för att detektera om viss data finns. Korta och enkla kolumnnamn minskar risken för att problem uppstår. Det är vanligt att man erbjuder söktjänster där frågor mot olika kolumner skrivs in i webbadresser och det är bra om det kan göras utan speciella teckenkodningar.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.
</div>

<div class="note" title="2">
Kolumn 3 och 4, latitude och longitude, anges på “WGS84-format”. Se [Förtydligande Attribut](#fortydligande-attribut) för förklaring. Detta är det breda vedertagna koordinatsystem som används av många kommersiellt tillgängliga kart- och  navigationsverktyg. De flesta av dessa verktyg gör det enkelt att ange eller plocka ut koordinater för en plats i detta format.

För dataproducenten kan [OpenStreetMap](https://www.openstreetmap.org/) rekommenderas för att t.ex. hitta koordinater för en viss plats genom att placera en kartnål och kopiera kartnålens koordinat.

Den som kan använda koordinater från kommunens GIS-system - tala med din GIS- eller Kart- och mätenhet för att säkerställa att dina koordinater är i WGS84-formatet.
</div>

<div class="ms_datatable">

| #  | Kolumnnamn      | Datatyp                         | Exempel och förklaring |
| -- | :---------------:| :-------------------------------: | ---------------------- |
| 1  | source           | heltal                         | **Obligatoriskt** - Ange organisationsnummret utan mellanslag och bindesstreck för organisationen. Exempel för Borås Stad: “2120001561”. |
| 2  | id               | text                           | **Obligatoriskt** - Ange en unik och stabil identifierare för lekplatsen inom er kommun. Det viktiga är att identifieraren är unik (tänk i hela världen), och att den är stabil över tid - d.v.s. att den aldrig kommer ändras. Om en lekplats avvecklas ska identifieraren inte återanvändas på en annan lekplats. Läs mer om [id](#id). <br>|
| 3  | name            | text                            | **Obligatoriskt** - Lekplatsens namn, exempel för en lekplats i Trollhättans Stad “Skrotnisses lekplats” eller för en lekplats i Mölndals Stad “Brovaktareleken”. <br>|
| 4  | type            | N &vert; O &vert; S             | **Obligatoriskt** - Information om vilken typ av lekplats som är beskriven. Kan beskrivas enligt närlekplats (N), områdeslekplats (O), eller stadsdelslekplats (S). Se [förtydligande](#type). <br> |
| 5  | latitude        | decimaltal                      | **Obligatoriskt** - Latitud anges per format som [WGS84](#latitud). <br>|
| 6  | longitude       | decimaltal                      | **Obligatoriskt** - Longitud anges per format som [WGS84](#longitud).<br>|
| 7  | email           | text                            | **Obligatoriskt** - E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Ange ej det inledande URI schemat `mailto:` eller HTML-koder i uttrycket. Exempel: info@molndal.se <br>|
| 8  | alt_source      | heltal                          | Ange SCB:s kommunkod för er kommun. Exempel: "1490" för Borås Stad. |
| 9  | wikidata        | text                            | Referens till Wikidata-artikel/entry om platsen. Se [Wikidata](#wikidata). Använd den sk. Q-koden till entiteten på Wikidata.|
| 10 | visit_url       | URL                             | Länk till allmän besöksinformation för platsen, t.ex. turistinformation eller liknande.|
| 11 | updated         | datum                           | Ett datum som anger när informationen om lekplatsen senast uppdaterades. Se [förtydligande](#fortydligande-attribut).|
| 12 | description     | text                            | Kort beskrivning av platsen och omgivningen. Undvik att beskriva sådant som redan framgår i specifikationen. |
| 13 | street          | text                            | Gatuadress till platsen, exempelvis “Bastugatan”. Ange inte nummer på gata eller fastighet, det anges i attributet “housenumber”. Mappar mot OpenStreetMaps “addr:street”. Läs  mer hos [OSM](httpsKey:addr). <br>|
| 14 | housenumber     | text                            | Gatunummer eller husnummer med bokstav. Exempelvis “1” eller “42 E”. Mappar mot OpenStreetMaps “addr:housenumber”. Läs mer hos [OSM](https://wiki.openstreetmap.org/wiki/Key:addr). <br>|
| 15 | postcode        | text                            | Postnummer, exempelvis “46430”, mappar mot OpenStreetMaps “addr:postcode”. Läs mer hos [OSM](https://wiki.openstreetmap.org/wiki/Key:addr). <br>|
| 16 | city            | text                            | Postort, exempelvis “Mellerud”. Mappar mot OpenStreetMaps “addr:city”. Läs mer hos [OSM](https://wiki.openstreetmap.org/wiki/Key:addr). <br>|
| 17 | country         | text                            | Land där lekplatsen finns. Skall anges enligt [ISO3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). För Sverige ange “SE”. Fältet mappar mot OpenStreetMaps “[addr:country](https://wiki.openstreetmap.org/wiki/Key:addr:country)”. <br>|
| 18 | lighting        | boolean                         | Beskriver om lekplatsen har belysning. |
| 19 | structure       | boolean                         | Komplex installation av anslutna lekplatsanordningar som kan bestå av plattformar, torn, broar, till vilket ytterligare komponenter kan tilläggas. <br> Se [bild](https://wiki.openstreetmap.org/wiki/File:SunwardCohousingPlayStructure2005.jpg)|
| 20 | sandpit         | boolean                         | Beskriver om lekplatsen har en sandlåda.|
| 21 | swing           | boolean                         | Beskriver om lekplatsen har en eller flera gungor. |
| 22 | basketswing     | boolean                         | Beskriver om lekplatsen har en eller flera större gungor med plats för mer än en person. Se [bild](https://wiki.openstreetmap.org/wiki/File:Accessibleplay-Hammockswing.jpg).|
| 23 | zipwire         | boolean                         | Beskriver om lekplatsen har en linbana. Se [bild](https://wiki.openstreetmap.org/wiki/File:Ropeway_play.jpg). |
| 24 | slide           | boolean                         | Beskriver om lekplatsen har en eller flera rutschkanor.|
| 25 | roundabout      | boolean                         | Beskriver om lekplatsen har en karusell. Se [bild](https://wiki.openstreetmap.org/wiki/File:Manually_powered_carousel_on_a_playground_in_Saint-Petersburg.JPG). |
| 26 | springy         | boolean                         | Beskriver om lekplatsen har en fjädergunga. Se [bild](https://wiki.openstreetmap.org/wiki/File:Springy_horse.jpg). |
| 27 | toilet          | boolean                         | Beskriver om toalett av någon typ finns på platsen - spoltoalett, torrdass, kemtoa et.c.|
| 28 | drinking_water  | boolean                         | Beskriver om det finns dricksvatten på platsen. Attributet beskriver förekomst av, inte på vilket sätt (slang, kran, pump, hink, källa osv).|
| 29 | wheelchair      |yes/no/limited                   | Beskriver till vilken grad lekplatsen är anpassad för rullstolar. Se [förtydligande](#wheelchair).|
| 30 | accessibility   |text                             | Ytterligare beskrivning om hur tillgänglighetsanpassad lekplatsen är.|
| 31 | td_url          | URL                             | Länk till [Tillgänglighetsdatabasen (TD)](#tillganglighetsdatabasen).|
</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.

### **decimaltal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas istället (då undviks problem med CSV formatet som använder komma som separator).

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**
En URL är en webbadress enligt specifikationen RFC 1738. I detta sammanhang förutsätts schemana http eller https. För webbadresser med speciella tecken tillåts UTF-8-encodade domäner och pather, se då RFC 3986.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "http://www.example.com" ok. Relativa webbadresser accepteras inte heller. (En fullständig regular expression utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**
Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad “Boolesk” datatyp och kan antingen ha ett av två värden: Sant eller Falskt, men aldrig båda.

### **phone**
Reguljärt uttryck: **`/^\+?[\d-]{6,12}$/`**

För bästa internationella funktion rekommenderas att ange telefonnummer i internationellt format enligt [RFC3966](https://www.ietf.org/rfc/rfc3966). Då anger du landskod med + före riktnummer, utan inledande nolla på riktnummer. Telefonnummer till Borås Stad växel, 033-357000 anges internationell på följande sätt: `+46-33-357000`

Andra godtagbara och möjliga format, istället för internationellt nummer: `033-357000`

**Observera** att man inte får inkludera mellanslag i telefonummret då man vill kunna generera en webbadress (URI) utifrån numret vilket inte går om det är mellanslag i det.
</div>

## Förtydligande attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Observera att “id” inte är namnet på lekplatsen utan dess identifierare. I fältet “name” skriver du in precis hur du vill att lekplatsen ska representeras i text. “id” är enbart till för att skapa en stabil identifiering som används när man ska låta en dator hantera informationen. Du kan jämföra attributen “id” och “name” som personnummer och namn för en människa. id är personnummer och identifierar exakt en person. "name" är namn på personen och flera människor kan heta Ellen Ripley, men alla har ett unikt personnummer. Det är mycket viktigt att detta attribut är stabilt över tid. Det ska inte aldrig ändras. Om en lekplats flyttas, stängs eller byter namn är det OK att skapa ett nytt id men tillse att det gamla IDt slutar att existera och aldrig återanvänds.

Om du redan har en unik och stabil identifierare för din lekplats i ett eget system, t.ex. ett GIS-system, ett anläggningsregister för fritidsområden, ett beskrivningsID för platsen i ett skötselsystem för en parkavdelning eller motsvarande, kan du använda denna istället som id. Du ansvarar för att namnet är unikt och stabilt över tid. Ett ID från ett verksamhetssystem inom kommunen duger gott, om du tillser att det är just unikt. Tre exempel på ID:n från verksamhetssystem inom en fiktiv kommun:

* urn:ngsi-ld:Playground:se:boras:anlaggning:108 
* trollhattan.se-anlaggningsid-GH130-55631
* vara-lekplats-Y031lumbersparklekplats

Om du inte har en unik och stabil identifierare för din lekplats redan, rekommenderas du att skapa en genom att ange en kommunkod från SCB för den kommun där lekplatsen finns, följt av ett bindestreck “-” och namnet på lekplatsen utan mellanslag och utan svenska tecken. Använd A-O och a-o, inga apostrofer, accenter, skiljetecken. Kommunkod anges alltid med fyra siffror med inledande nolla om sådan finns: <br>
`[kommunkod]-[NamnUtanSvenskaTeckenEllerMellanslag]`

Uttryckt som ett reguljärt uttryck blir detta: **`/^\d{4}-[a-zA-Z]+$/`**

Exempel “0854-Lekplats” 

För lekplatser utanför Sverige som saknar kommunkod, anger du 9999. Exempel för ett lekplats i Norge: 9999-BirkeveienLekeplass

Unik och stabil identifierare för lekplatsen. Består förslagsvis av kommunkoden (4 siffror), ett bindestreck “-” och sedan namnet på lekplatsen utan Å, Ä, Ö, bindestreck, mellanslag eller andra tecken som apostrof et.c. Endast bokstäver a-z och A-Z är tillåtet.

Exempel på icke tillåtna namn:

* 0854-Lilla Ekarängen
* 0854-LillaEkarängen
* 0854-Lilla Ekarängen Fessingsgatan 1
* lekplats 002 Borås Stad
* Lilla Ekarängen Borås i Sverige
* 021 023 - Anläggning 12
* boras.se/lekplats/Lilla Ekarängen

Ovanstående namn korrigerade att bli giltiga:

* 0854-LillaEkarangen
* 0854-LillaEkarangen
* 0854-LillaEkarangenFessingsgatan
* lekplats002BorasStad
* LillaEkarangenBorasiSverige
* 021_023_Anlaggning_12
* boras.se/lekplats/lilla_ekarangen

Överväg att ha en global och stabil identifierare via [Wikidata](#wikidata) som kompletterar fältet [id](#id). Läs mer vid förklaringen om [Wikidata](#wikidata).

### **latitud**

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges.

### **longitud**

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.

### **email**

E-postadress ska anges enligt formatspecifikation i [RFC5322](https://datatracker.ietf.org/doc/html/rfc5322) i sektionen “addr-spec”. Verifiera att du följer denna standard.  

### **visit_url**

Ange lämpligen den ingångsidan för lekplatser på kommunens eller turistbolagets webbsida. Det här är den ingångssida som du önskar att besökare skall komma till för att lära sig mer om lekplatser i hela kommunen. Exempel på en sådan sida i Borås Stad - [https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen](https://www.boras.se/upplevaochgora/lekplatserochaktivitetsplatser/lekplatser/lillaekarangen).

Vi rekommenderar starkt att du använder detta attribut för att leda besökare till mer information kring lekplats i kommunen. 

### **type**

Attributet beskriver lekplatsens typ och har syftet att ge möjlighet till att veta hur mycket människor som kan befinna sig vid lekplatsen. Bör beskrivas som närlekplats (N), områdeslekplats (O), eller stadsdelslekplats (S). 

Närlekplats bör anges när lekplatsen är av det mindre slaget och främst ska nyttjas av boende i närheten. Områdeslekplats bör anges när lekplatsen kan nyttjas av ett helt bostadsområde. Stadsdelslekplats bör anges när det handlar om en väldigt stor lekplats med kapacitet för många människor, som kan vara värd att göra en utflykt till.

### **wheelchair**

Attributet beskriver till vilken grad en lekplats är tillgänglighetsanpassad för rullstolar. Här anges "yes" i det fallet att man kan utnyttja hela lekplatsen med rullstol, d.v.s. att det inte finns hinder i form av trappor eller kanter för att ta sig in på och nyttja hela lekplatsen. 

"limited" anges i det fall vissa delar av lekplatsen kan utnyttjas men inte hela området. "no" anges om man inte kan alls med rullstol kan komma åt lekplatsens ytor. Ta reda på status eller lämna tomt vid första publicering om data inte finns vid tillfället.

### **updated**

Detta attribut ska anges när någon del av informationen har uppdaterats, hur liten och till synes obetydlig uppdateringen är. Utan datum blir det svårt för en implementatör att veta hur aktuell datat är och ifall det går att lita på. Datum ska anges på format enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) utan undantag. 

Exempel, onsdagen den 21:e juli 2021 ska anges enligt:
2021-07-21

### **description**

Du behöver inte skriva att lekplatsen har en sandlåda då det framgår genom att sätta true i attribut nr 20 “sandpit”. 

Exempel på beskrivning: “lekplats beläget nära naturområden med leder att vandra.”

Vill du dela data på flera olika språk? Se [språkstöd](#sprakstod) och [beskrivning i datakatalog](#beskrivning-i-datakatalog) för att lära dig mer om kraven på metadata i samband med publicering. 

### **td_url**

Det finns inga unika ID:n att länka till utan du länkar till hela den webbadress (URL) som går direkt till ett objekt som är upplagt i Tillgänglighetsdatabasen. 
